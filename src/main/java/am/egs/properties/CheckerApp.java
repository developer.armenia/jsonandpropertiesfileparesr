package am.egs.properties;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

public class CheckerApp {

    private static List<String> prodLocaleArray = Arrays.asList("amersud-eurest-gcc", "be", "beta", "de", "es", "fr", "gcc", "it", "ma", "mp-excw", "mpce", "mpsc", "mpsl", "nl", "pl", "pt", "ro", "ru", "tr");
    //private static List<String> prodLocaleArray = Arrays.asList("amersud-eurest-gcc", "be", "chat", "de",  "es", "fr", "gcc", "tr");

    private static String initShopCountry;
    private static String initShopListLocale;

    private static JsonElement root = null;

    public static void main(String[] args) {
        for(String prod : prodLocaleArray) {
            System.out.println("prod="+prod);
            initStoreCoreValues(prod);
            String[] countryList = initShopCountry.split("#");
            String[] localeList = initShopListLocale.split("#");

            String country;
            String locale;
            for (int i = 0; i < countryList.length; i++) {
                country = countryList[i];
                locale = localeList[i];
                //System.out.print("Given Country "+country.toLowerCase());
                String s = initWebAppValues(country.toLowerCase());

                if (s.length() > 0) {
                    String resultLocal = s.substring(0, s.length() - 1);
                    //System.out.println(s);

                    if (!locale.equals(resultLocal)) {
                        System.out.println("Wrong: Given country="+country+ " locale=" + locale + " EXPECTED local=" + resultLocal);
                    }
                } else {
                    System.out.println("Wrong: Given country="+country+ " locale=" + locale + " EXPECTED local=" + s);
                }
            }
        }

    }

    public static String initWebAppValues(String lang){
        String result = "";
        try {
            if(root == null) {
                root = new JsonParser().parse(new FileReader("G:\\repo\\rcs-store-web-app\\config\\prod\\app.config.json"));
            }
            JsonArray object = root.getAsJsonObject().get("AppConfig").getAsJsonObject().get(lang).getAsJsonObject().get("locales").getAsJsonArray();

            for(Object obj : object){
                result += obj.toString().replace("\"", "") + ",";
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
        }
        return result;
    }


    public static void initStoreCoreValues(String env){
        try (InputStream input = new FileInputStream("G:\\repo\\rcs-store-core\\lsf-renault\\lsf-renault-webapp\\src\\main\\filters\\"+env+"\\prod.properties")) {

            Properties prop = new Properties();

            // load a properties file
            prop.load(input);

            // get the property value and print it out

            initShopCountry = prop.getProperty("confShop.initShopCountry");
            initShopListLocale = prop.getProperty("confShop.initShopListLocale");


            //System.out.println(initShopCountry);
            //System.out.println(initShopListLocale);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

}

